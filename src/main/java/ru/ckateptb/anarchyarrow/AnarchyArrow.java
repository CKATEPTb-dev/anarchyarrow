package ru.ckateptb.anarchyarrow;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import ru.ckateptb.anarchyarrow.arrow.*;
import ru.ckateptb.anarchyarrow.command.AnarchyCommand;
import ru.ckateptb.anarchyarrow.config.Configuration;
import ru.ckateptb.anarchyarrow.listener.Handler;
import ru.ckateptb.tablefeature.recipe.RecipeBuilder;

import static ru.ckateptb.anarchyarrow.arrow.AnarchyArrowFactory.ANARCHY_ARROW_ITEM_STACK_MAP;

public final class AnarchyArrow extends JavaPlugin {

    private static AnarchyArrow instance;

    public static AnarchyArrow getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        load();
        new AnarchyCommand("anarchyarrow", "aa");
        Bukkit.getPluginManager().registerEvents(new Handler(), this);
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, IAnarchyArrow.instances::notifyObservers, 50, 0);
        if (!Configuration.DISABLE_CRAFTING) {
            ItemStack arrow = new ItemStack(Material.ARROW);
            ItemStack tnt = new ItemStack(Material.TNT);
            createBoxRecipe(tnt, arrow, LiteExplosionAnarchyArrow.item).buildShapeless();
            createBoxRecipe(tnt, LiteExplosionAnarchyArrow.item, TempExplosionAnarchyArrow.item).buildStrictShaped();
            createBoxRecipe(new ItemStack(Material.FEATHER), arrow, AirAnarchyArrow.item).buildShaped();
            createBoxRecipe(new ItemStack(Material.POISONOUS_POTATO), arrow, ConfusionAnarchyArrow.item).buildShaped();
            createBoxRecipe(new ItemStack(Material.INK_SACK), arrow, DarknessAnarchyArrow.item).buildShaped();
            createBoxRecipe(new ItemStack(Material.POISONOUS_POTATO), ConfusionAnarchyArrow.item, DeathAnarchyArrow.item).buildStrictShaped();
            createBoxRecipe(new ItemStack(Material.DIRT), arrow, EarthAnarchyArrow.item).buildShaped();
            createBoxRecipe(new ItemStack(Material.ENDER_PEARL), arrow, EnderAnarchyArrow.item).buildShaped();
            createBoxRecipe(new ItemStack(Material.TRIPWIRE_HOOK), arrow, GrappleAnarchyArrow.item).buildShaped();
            createBoxRecipe(new ItemStack(Material.MELON), arrow, LifeAnarchyArrow.item).buildShaped();
            createBoxRecipe(new ItemStack(Material.BLAZE_POWDER), arrow, MagicAnarchyArrow.item).buildShaped();
            new RecipeBuilder(this, ExplosionAnarchyArrow.item).withFirstLine(arrow, tnt, null).buildShapeless();
        }
    }

    private RecipeBuilder createBoxRecipe(ItemStack center, ItemStack border, ItemStack result) {
        ItemStack stack = result.clone();
        stack.setAmount(8);
        return new RecipeBuilder(this, stack)
                .withFirstLine(border, border, border)
                .withSecondLine(border, center, border)
                .withThirdLine(border, border, border);
    }

    public void load() {
        new Configuration(this, "config");
        ANARCHY_ARROW_ITEM_STACK_MAP.clear();
        ExplosionAnarchyArrow.onLoad();
        TempExplosionAnarchyArrow.onLoad();
        LiteExplosionAnarchyArrow.onLoad();
        AirAnarchyArrow.onLoad();
        ConfusionAnarchyArrow.onLoad();
        DarknessAnarchyArrow.onLoad();
        DeathAnarchyArrow.onLoad();
        EarthAnarchyArrow.onLoad();
        EnderAnarchyArrow.onLoad();
        GrappleAnarchyArrow.onLoad();
        LifeAnarchyArrow.onLoad();
        MagicAnarchyArrow.onLoad();
    }
}