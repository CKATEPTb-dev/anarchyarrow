package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.config.Configuration;
import org.apache.commons.lang3.RandomUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.collision.Sphere;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class AirAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_AIR_NAME)
                .withLore(Configuration.ARROW_AIR_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_AIR_NAME, AirAnarchyArrow.class);
    }

    private final Arrow arrow;

    public AirAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity != null) {
            if (entity instanceof LivingEntity) {
                LivingEntity target = (LivingEntity) entity;
                target.damage(event.getFinalDamage(), event.getDamager());
                target.setVelocity(target.getVelocity().setY(Configuration.ARROW_AIR_POWER));
                target.getWorld().playSound(target.getLocation(), Sound.ITEM_BUCKET_EMPTY, 1, 2);
                event.setCancelled(true);
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Arrow arrow = getArrow();
        if (arrow.isDead()) {
            unregister();
            return;
        }
        Location origin = arrow.getLocation();
        if (origin.getBlock().isLiquid()) {
            Sphere sphere = new Sphere(origin, Configuration.ARROW_AIR_REPLENISH_RADIUS);
            sphere.handleEntityCollisions(entity -> {
                LivingEntity le = (LivingEntity) entity;
                if (le.getRemainingAir() <= le.getMaximumAir()) {
                    le.setRemainingAir(le.getRemainingAir() + Configuration.ARROW_AIR_REPLENISH_POWER / 20);
                }
                return false;
            }, origin, true);
            sphere.handleBlockCollisions().forEach(block -> {
                if (RandomUtils.nextBoolean()) {
                    Location location = block.getLocation();
                    Material type = block.getType();
                    if (type.equals(Material.WATER) || type.equals(Material.STATIONARY_WATER)) {
                        Particle.WATER_BUBBLE.display(location, 0.5f, 0.5f, 0.5f, 0, 1);
                    }
                }
            });
        }
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
