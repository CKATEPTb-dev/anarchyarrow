package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.AnarchyArrow;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;
import java.util.Observer;
import java.util.Optional;

public class AnarchyArrowFactory {

    //TODO SHOULD TO USE NBT TAG, NOT ARROW_NAME
    public static Map<String, Class<? extends IAnarchyArrow>> ANARCHY_ARROW_ITEM_STACK_MAP = new HashMap<>();

    //register after server startup
    public static void register(String name, Class<? extends IAnarchyArrow> anarchyArrow) {
        Bukkit.getScheduler().runTaskLater(AnarchyArrow.getInstance(),
                () -> ANARCHY_ARROW_ITEM_STACK_MAP.put(name, anarchyArrow), 0);
    }

    public static Optional<IAnarchyArrow> createByFirstArrow(Player player, Arrow arrow) {
        PlayerInventory inventory = player.getInventory();
        int slot = inventory.first(Material.ARROW);
        if (slot != -1) {
            ItemStack itemStack = inventory.getItem(slot);
            ItemMeta itemMeta = itemStack.getItemMeta();
            if (itemMeta != null) {
                String name = itemMeta.getDisplayName();
                Class<? extends IAnarchyArrow> clazz = ANARCHY_ARROW_ITEM_STACK_MAP.get(name);
                if (clazz != null) {
                    try {
                        return Optional.of(clazz.getConstructor(Arrow.class).newInstance(arrow));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return Optional.empty();
    }

    public static Optional<IAnarchyArrow> get(Arrow arrow) {
        for (Observer observer : IAnarchyArrow.instances.getObservers()) {
            IAnarchyArrow anarchyArrow = (IAnarchyArrow) observer;
            if (anarchyArrow.getArrow() == arrow) {
                return Optional.of(anarchyArrow);
            }
        }
        return Optional.empty();
    }
}
