package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.config.Configuration;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class ConfusionAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_CONFUSION_NAME)
                .withLore(Configuration.ARROW_CONFUSION_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_CONFUSION_NAME, ConfusionAnarchyArrow.class);
    }

    private final Arrow arrow;

    public ConfusionAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
    }


    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity != null) {
            if (entity instanceof Player) {
                Player player = (Player) entity;
                player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, (int) (Configuration.ARROW_CONFUSION_DURATION / 50), 0));
                Location backwards = player.getLocation();
                backwards.setYaw(player.getLocation().getYaw() + 180);
                backwards.setPitch(player.getLocation().getPitch() + 180);
                player.teleport(backwards);
            }
            if (entity instanceof Creature) {
                ((Creature) entity).setTarget(null);
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Arrow arrow = getArrow();
        if (arrow.isDead()) {
            unregister();
            return;
        }
        Particle.SPELL.display(getArrow().getLocation(), 0.1F, 0.1F, 0.1F, 0, 2);
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
