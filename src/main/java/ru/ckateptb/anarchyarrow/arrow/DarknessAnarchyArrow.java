package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.config.Configuration;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.collision.Sphere;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class DarknessAnarchyArrow implements IAnarchyArrow {

    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_DARKNESS_NAME)
                .withLore(Configuration.ARROW_DARKNESS_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_DARKNESS_NAME, DarknessAnarchyArrow.class);
    }

    private final Arrow arrow;
    private long startTime = -1;

    public DarknessAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
        this.startTime = System.currentTimeMillis();
        Location origin = getArrow().getLocation();
        origin.getWorld().playSound(origin, Sound.ENTITY_GENERIC_EXTINGUISH_FIRE, 1, 0.5F);
        Particle.EXPLOSION_NORMAL.display(origin, 0f, 0f, 0f, 0, 10);
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {
    }

    @Override
    public void update(Observable o, Object arg) {
        Arrow arrow = getArrow();
        if (arrow.isDead()) {
            unregister();
            return;
        }
        if (startTime == -1) {
            Particle.SMOKE_LARGE.display(arrow.getLocation(), 0.1f, 0.1f, 0.1f, 0.1f, 1);
        } else if (System.currentTimeMillis() - startTime < Configuration.ARROW_DARKNESS_DURATION) {
            Location location = arrow.getLocation();
            Sphere sphere = new Sphere(location, Configuration.ARROW_DARKNESS_RADIUS);
            sphere.handleBlockCollisions().forEach(block -> {
                if (block.getType().isTransparent()) {
                    Particle.SMOKE_LARGE.display(block.getLocation(), 0.5f, 0.5f, 0.5f, 0.1f, 5);
                }
            });
        }
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
