package ru.ckateptb.anarchyarrow.arrow;

import org.apache.commons.lang3.RandomUtils;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.ckateptb.anarchyarrow.config.Configuration;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class DeathAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_DEATH_NAME)
                .withLore(Configuration.ARROW_DEATH_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_DEATH_NAME, DeathAnarchyArrow.class);
    }

    private final Arrow arrow;

    public DeathAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
    }


    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity != null) {
            if (entity instanceof LivingEntity) {
                if (RandomUtils.nextInt(0, 100) < Configuration.ARROW_DEATH_CHANCE) {
                    if (arrow.getShooter() instanceof Entity) {
                        ((LivingEntity) entity).damage(((LivingEntity) entity).getHealth(), (Entity) arrow.getShooter());
                    } else {
                        ((LivingEntity) entity).damage(((LivingEntity) entity).getHealth());
                    }
                } else {
                    LivingEntity le = (LivingEntity) entity;
                    le.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, (int) (Configuration.ARROW_DEATH_DURATION / 50), Configuration.ARROW_DEATH_POWER - 1));
                }
            }
            if (entity instanceof Creature) {
                ((Creature) entity).setTarget(null);
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Arrow arrow = getArrow();
        if (arrow.isDead()) {
            unregister();
            return;
        }
        Particle.DAMAGE_INDICATOR.display(getArrow().getLocation(), 0.1F, 0.1F, 0.1F, 0, 2);
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
