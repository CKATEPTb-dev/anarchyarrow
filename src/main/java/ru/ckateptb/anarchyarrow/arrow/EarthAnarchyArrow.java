package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.config.Configuration;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.ckateptb.tablefeature.block.temp.TempFallingBlock;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class EarthAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    private TempFallingBlock fallingBlock;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_EARTH_NAME)
                .withLore(Configuration.ARROW_EARTH_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_EARTH_NAME, EarthAnarchyArrow.class);
    }

    private final Arrow arrow;

    public EarthAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity != null) {
            if (entity instanceof LivingEntity) {
                target = (LivingEntity) entity;
                long duration = Configuration.ARROW_EARTH_DURATION;
                this.fallingBlock = new TempFallingBlock(target.getLocation().add(0, 0.3, 0), Material.DIRT.getNewData((byte) 0))
                        .withDuration(duration)
                        .withGravity(false);
                this.fallingBlock.place();
                target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (duration / 50), 255));
                target.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, (int) (duration / 50), 250));
            }
        }
    }

    private LivingEntity target;

    @Override
    public void update(Observable o, Object arg) {
        Arrow arrow = getArrow();
        if (arrow.isDead()) {
            unregister();
        }
        if (fallingBlock != null) {
            Location location = fallingBlock.getFallingBlock().getLocation();
            location.setYaw(target.getLocation().getYaw());
            location.setPitch(target.getLocation().getPitch());
            target.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
        } else {
            Particle.BLOCK_CRACK.display(arrow.getLocation(), 0.1f, 0.1f, 0.1f, 0, 5, Material.DIRT.getNewData((byte) 0));
        }
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
