package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.config.Configuration;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class EnderAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_ENDER_NAME)
                .withLore(Configuration.ARROW_ENDER_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_ENDER_NAME, EnderAnarchyArrow.class);
    }

    private final Arrow arrow;

    public EnderAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
        Block block = event.getHitBlock();
        if (block != null) {
            ProjectileSource shooter = arrow.getShooter();
            if (!(shooter instanceof LivingEntity)) {
                return;
            }

            arrow.remove();
            Location teleportLocation = block.getLocation().add(0.5, 1, 0.5);
            ((LivingEntity) shooter).teleport(teleportLocation);
        }
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity != null) {
            if (entity instanceof LivingEntity) {
                ProjectileSource source = arrow.getShooter();
                if (!(source instanceof LivingEntity) || entity.getType() == EntityType.ARMOR_STAND) {
                    return;
                }
                this.swapLocations((LivingEntity) source, (LivingEntity) entity);
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Arrow arrow = getArrow();
        if (arrow.isDead()) {
            unregister();
            return;
        }
        Particle.PORTAL.display(arrow.getLocation(), 0.1f, 0.1f, 0.1f, 0, 3);
    }

    private void swapLocations(LivingEntity shooter, LivingEntity target) {
        arrow.setKnockbackStrength(0);

        Location targetLocation = target.getLocation();
        Vector targetVelocity = target.getVelocity();

        // Swap player locations
        target.teleport(shooter.getLocation());
        target.setVelocity(shooter.getVelocity());
        shooter.teleport(targetLocation);
        shooter.setVelocity(targetVelocity);

        // Play sounds and display particles
        World world = arrow.getWorld();
        world.playSound(arrow.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 3);
        world.playSound(target.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 3);
        Particle.PORTAL.display(arrow.getLocation(), 1, 1, 1, 0, 50);
        Particle.PORTAL.display(target.getLocation(), 1, 1, 1, 0, 5);
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
