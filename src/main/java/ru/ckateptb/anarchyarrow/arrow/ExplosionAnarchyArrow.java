package ru.ckateptb.anarchyarrow.arrow;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Arrow;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.anarchyarrow.config.Configuration;
import ru.ckateptb.tablefeature.builders.ItemBuilder;

public class ExplosionAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_EXPLOSION_NAME)
                .withLore(Configuration.ARROW_EXPLOSION_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_EXPLOSION_NAME, ExplosionAnarchyArrow.class);
    }

    private final Arrow arrow;

    public ExplosionAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
        Location origin = getArrow().getLocation();
        float power = Configuration.ARROW_EXPLOSION_POWER;
        World world = origin.getWorld();
        world.createExplosion(arrow.getLocation(), power);
        arrow.remove();
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {

    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
