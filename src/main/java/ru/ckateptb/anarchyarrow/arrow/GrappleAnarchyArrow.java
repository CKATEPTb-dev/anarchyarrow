package ru.ckateptb.anarchyarrow.arrow;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;
import ru.ckateptb.anarchyarrow.config.Configuration;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class GrappleAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_GRAPPLE_NAME)
                .withLore(Configuration.ARROW_GRAPPLE_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_GRAPPLE_NAME, GrappleAnarchyArrow.class);
    }

    private final Arrow arrow;
    private LivingEntity shooter;

    public GrappleAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
        arrow.remove();
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        if(event.getEntity() == arrow.getShooter()) event.setCancelled(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (getArrow().isDead()) {
            unregister();
            return;
        }
        if (!arrow.isInBlock()) {
            ProjectileSource source = arrow.getShooter();
            if (!(source instanceof LivingEntity)) {
                return;
            }
            this.shooter = (LivingEntity) source;
            if (shooter instanceof Player && ((Player) shooter).isSneaking()) {
                arrow.remove();
            } else {
                Location location = arrow.getLocation();
                Particle.CRIT.display(location, 0, 0, 0, 0.1f, 3);
                shooter.setVelocity(arrow.getVelocity().multiply(1));
                arrow.getWorld().playSound(location, Sound.ENTITY_BAT_TAKEOFF, 1, 2);
            }
            if (Configuration.ARROW_GRAPPLE_DISABLE_FALL_DAMAGE) {
                shooter.setFallDistance(0);
            }
        }
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
