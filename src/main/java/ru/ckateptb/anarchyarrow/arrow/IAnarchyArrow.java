package ru.ckateptb.anarchyarrow.arrow;

import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import ru.ckateptb.tablefeature.utils.ForceObservable;

import java.util.Observable;
import java.util.Observer;

public interface IAnarchyArrow extends Observer {
    ForceObservable instances = new ForceObservable();

    default void register() {
        instances.addObserver(this);
    }

    void onHit(ProjectileHitEvent event);

    void onHit(EntityDamageByEntityEvent event);

    default void unregister() {
        instances.deleteObserver(this);
    }

    Arrow getArrow();

    ItemStack getItem();

    default void onPickUp(PlayerPickupArrowEvent event) {
        event.setCancelled(true);
        event.getArrow().remove();
        event.getPlayer().getInventory().addItem(getItem());
        unregister();
    }

    @Override
    default void update(Observable o, Object arg) {
        if (getArrow().isDead()) {
            unregister();
        }
    }

    default boolean containsAnarchyArrow(ItemStack itemStack, Player player) {
        PlayerInventory inventory = player.getInventory();
        int tempExplosionArrow = first(itemStack, inventory);
        int arrow = inventory.first(Material.ARROW);
        int spectralArrow = inventory.first(Material.SPECTRAL_ARROW);
        int tippedArrow = inventory.first(Material.TIPPED_ARROW);
        if (arrow != -1 && Math.min(tempExplosionArrow, arrow) == arrow && arrow != tempExplosionArrow) return false;
        if (spectralArrow != -1 && Math.min(tempExplosionArrow, spectralArrow) == spectralArrow) return false;
        if (tippedArrow != -1 && Math.min(tempExplosionArrow, tippedArrow) == tippedArrow) return false;
        return tempExplosionArrow != -1;
    }

    default int first(ItemStack item, Inventory inv) {
        if (item == null) {
            return -1;
        } else {
            ItemStack[] inventory = inv.getStorageContents();
            int i = 0;
            while (true) {
                if (i >= inventory.length) {
                    return -1;
                }
                if (inventory[i] != null) {
                    if (item.isSimilar(inventory[i])) {
                        break;
                    }
                }
                ++i;
            }
            return i;
        }
    }

}
