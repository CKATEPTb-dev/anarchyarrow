package ru.ckateptb.anarchyarrow.arrow;

import net.minecraft.server.v1_12_R1.BlockState;
import net.minecraft.server.v1_12_R1.TileEntity;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.ckateptb.anarchyarrow.config.Configuration;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.collision.Sphere;
import ru.ckateptb.tablefeature.particle.Particle;

public class LifeAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_LIFE_NAME)
                .withLore(Configuration.ARROW_LIFE_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_LIFE_NAME, LifeAnarchyArrow.class);
    }

    private final Arrow arrow;

    public LifeAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
        Location origin = arrow.getLocation();
        new Sphere(origin, Configuration.ARROW_LIFE_RADIUS).handleEntityCollisions(entity -> {
            LivingEntity le = (LivingEntity) entity;
            le.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, (int) (Configuration.ARROW_LIFE_DURATION / 50), Configuration.ARROW_LIFE_POWER - 1), true);
            Particle.HEART.display(le.getLocation().add(0, 3, 0), 0, 0, 0, 0, 1);
            return false;
        }, origin, true);
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity != null) {
            if (entity instanceof LivingEntity) {
                arrow.setKnockbackStrength(0);
                event.setDamage(0);
                event.setCancelled(true);
                arrow.remove();
            }
        }
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
