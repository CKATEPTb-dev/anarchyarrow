package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.config.Configuration;
import org.bukkit.*;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.collision.Sphere;
import ru.ckateptb.tablefeature.utils.VectorUtils;

public class LiteExplosionAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_LITE_EXPLOSION_NAME)
                .withLore(Configuration.ARROW_LITE_EXPLOSION_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_LITE_EXPLOSION_NAME, LiteExplosionAnarchyArrow.class);
    }

    private final Arrow arrow;

    public LiteExplosionAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
        Location origin = getArrow().getLocation();
        double damage = Configuration.ARROW_LITE_EXPLOSION_DAMAGE;
        double power = Configuration.ARROW_LITE_EXPLOSION_POWER;
        double radius = Configuration.ARROW_LITE_EXPLOSION_RADIUS;
        if (damage + power > 0) {
            Sphere sphere = new Sphere(origin, radius);
            sphere.handleEntityCollisions(entity -> {
                LivingEntity livingEntity = (LivingEntity) entity;
                if (damage > 0) {
                    ProjectileSource source = getArrow().getShooter();
                    if (source instanceof LivingEntity) {
                        final EntityDamageByEntityEvent finalEvent = new EntityDamageByEntityEvent((Entity) source,
                                entity, EntityDamageEvent.DamageCause.ENTITY_EXPLOSION, damage);
                        livingEntity.damage(damage, (Entity) source);
                        livingEntity.setLastDamageCause(finalEvent);
                    } else {
                        livingEntity.damage(damage);
                    }

                }
                if (power > 0) {
                    Location targetLocation = ((LivingEntity) entity).getEyeLocation();
                    Vector directional = VectorUtils.fromTo(origin.clone().subtract(0,1,0),targetLocation);
                    entity.setVelocity(directional.multiply(power));
                }
                return false;
            }, origin, true);
            World world = origin.getWorld();
            world.spawnParticle(Particle.EXPLOSION_LARGE, origin, 10);
            world.playSound(origin, Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
        }
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {

    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
