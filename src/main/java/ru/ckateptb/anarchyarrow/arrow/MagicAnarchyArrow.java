package ru.ckateptb.anarchyarrow.arrow;

import org.apache.commons.lang3.RandomUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import ru.ckateptb.anarchyarrow.config.Configuration;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.collision.Sphere;
import ru.ckateptb.tablefeature.particle.Particle;

import java.util.Observable;

public class MagicAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_MAGIC_NAME)
                .withLore(Configuration.ARROW_MAGIC_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_MAGIC_NAME, MagicAnarchyArrow.class);
    }

    private final Arrow arrow;

    public MagicAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity != null) {
            if (entity instanceof LivingEntity) {
                launchEntity((LivingEntity) entity);
            }
        }
    }

    private void launchEntity(LivingEntity entity) {
        Vector sourceVelocity = arrow.getVelocity();
        Vector targetVelocity = new Vector(sourceVelocity.getX() * 2, 0.75, sourceVelocity.getZ() * 2);

        if (Math.abs(targetVelocity.getX()) > 4 || Math.abs(targetVelocity.getY()) > 4 || Math.abs(targetVelocity.getZ()) > 4) {
            targetVelocity.normalize().multiply(4);
        }

        entity.setVelocity(targetVelocity);
        entity.getWorld().playSound(entity.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 1, 2);
    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
