package ru.ckateptb.anarchyarrow.arrow;

import ru.ckateptb.anarchyarrow.config.Configuration;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;
import ru.ckateptb.tablefeature.block.temp.TempExplosion;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.particle.Particle;

public class TempExplosionAnarchyArrow implements IAnarchyArrow {
    public static ItemStack item;

    public static void onLoad() {
        item = new ItemBuilder(Material.ARROW)
                .withDisplayName(Configuration.ARROW_TEMP_EXPLOSION_NAME)
                .withLore(Configuration.ARROW_TEMP_EXPLOSION_LORE)
                .withAmount(1)
                .build();
        AnarchyArrowFactory.register(Configuration.ARROW_TEMP_EXPLOSION_NAME, TempExplosionAnarchyArrow.class);
    }

    private final Arrow arrow;

    public TempExplosionAnarchyArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public void onHit(ProjectileHitEvent event) {
        Location origin = getArrow().getLocation();
        double damage = Configuration.ARROW_TEMP_EXPLOSION_DAMAGE;
        double power = Configuration.ARROW_TEMP_EXPLOSION_POWER;
        double radius = Configuration.ARROW_TEMP_EXPLOSION_RADIUS;
        World world = origin.getWorld();
        Particle.EXPLOSION_LARGE.display(origin, (float) radius / 4, 0, (float) radius / 4, 0, (int) (radius * 2));
        world.playSound(origin, Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
        TempExplosion explosion = new TempExplosion(origin, radius)
                .withDuration(Configuration.ARROW_TEMP_EXPLOSION_DURATION);
        if (damage + power > 0) {
            explosion.withCollisionCallback(entity -> {
                LivingEntity livingEntity = (LivingEntity) entity;
                if (damage > 0) {
                    ProjectileSource source = getArrow().getShooter();
                    if (source instanceof LivingEntity) {
                        final EntityDamageByEntityEvent finalEvent = new EntityDamageByEntityEvent((Entity) source,
                                entity, EntityDamageEvent.DamageCause.BLOCK_EXPLOSION, damage);
                        livingEntity.damage(damage, (Entity) source);
                        livingEntity.setLastDamageCause(finalEvent);
                    } else {
                        livingEntity.damage(damage);
                    }

                }
                if (power > 0) {
                    Location targetLocation = ((LivingEntity) entity).getEyeLocation();
                    Vector directional = targetLocation.subtract(origin.clone().add(0, -1, 0)).toVector().normalize();
                    entity.setVelocity(directional.multiply(power));
                }
                return false;
            });
        }
        explosion.place();
        arrow.remove();
    }

    @Override
    public void onHit(EntityDamageByEntityEvent event) {

    }

    @Override
    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }
}
