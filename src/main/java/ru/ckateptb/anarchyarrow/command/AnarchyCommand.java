package ru.ckateptb.anarchyarrow.command;

import ru.ckateptb.anarchyarrow.AnarchyArrow;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.anarchyarrow.arrow.*;
import ru.ckateptb.tablefeature.command.ForceCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnarchyCommand extends ForceCommand {
    public AnarchyCommand(String... command) {
        super(command);
    }

    @Override
    public boolean progress(CommandSender sender, String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "explosion": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.explosion")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = ExplosionAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "tempexplosion": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.tempexplosion")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = TempExplosionAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "liteexplosion": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.liteexplosion")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = LiteExplosionAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "air": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.air")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = AirAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "confusion": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.confusion")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = ConfusionAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "darkness": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.darkness")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = DarknessAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "death": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.death")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = DeathAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "earth": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.earth")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = EarthAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "ender": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.ender")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = EnderAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "grapple": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.grapple")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = GrappleAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "life": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.life")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = LifeAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "magic": {
                    if (isPlayer(sender)) {
                        if (sender.hasPermission("anarchyarrow.give.magic")) {
                            String amount = args.length > 1 ? args[1] : null;
                            ItemStack arrow = MagicAnarchyArrow.item.clone();
                            arrow.setAmount(NumberUtils.toInt(amount, 1));
                            ((Player) sender).getInventory().addItem(arrow);
                            return true;
                        }
                    }
                    break;
                }
                case "reload": {
                    if (sender.hasPermission("anarchyarrow.reload")) {
                        AnarchyArrow.getInstance().load();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean isPlayer(CommandSender sender) {
        if (sender instanceof Player) {
            return true;
        }
        sender.sendMessage("\u00a74You must be player");
        return false;
    }

    @Override
    public List<String> tab(CommandSender commandSender, String[] strings, List<String> list) {
        List<String> values = Arrays.asList("reload"
                , "Explosion"
                , "LiteExplosion"
                , "TempExplosion"
                , "Air"
                , "Confusion"
                , "Darkness"
                , "Death"
                , "Earth"
                , "Ender"
                , "Grapple"
                , "Life"
                , "Magic"
        );
        List<String> returned = new ArrayList<>();
        List<String> finalReturned = returned;
        values.forEach(s -> {
            if (s.toLowerCase().startsWith(strings[0].toLowerCase())) finalReturned.add(s);
        });
        returned = finalReturned;
        if (strings.length == 2) {
            returned = Arrays.asList("1", "16", "32", "64");
        }
        return returned;
    }
}
