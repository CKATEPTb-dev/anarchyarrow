package ru.ckateptb.anarchyarrow.config;

import org.bukkit.plugin.Plugin;
import ru.ckateptb.tablefeature.config.Hocon;

import java.util.Arrays;
import java.util.List;

public class Configuration extends Hocon {
    @Save({"disable_crafting"})
    public static boolean DISABLE_CRAFTING = false;
    @Save({"arrow", "explosion", "name"})
    public static String ARROW_EXPLOSION_NAME = "§5§lTNT стрела";
    @Save({"arrow", "explosion", "lore"})
    public static List<String> ARROW_EXPLOSION_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lВзрывается при столкновении");
    @Save({"arrow", "explosion", "power"})
    public static float ARROW_EXPLOSION_POWER = 2F;

    @Save({"arrow", "tempexplosion", "name"})
    public static String ARROW_TEMP_EXPLOSION_NAME = "§5§lВзрывная стрела";
    @Save({"arrow", "tempexplosion", "lore"})
    public static List<String> ARROW_TEMP_EXPLOSION_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lВзрывается при столкновении"
            , "§f§lОчень мощный взрыв");
    @Save({"arrow", "tempexplosion", "damage"})
    public static double ARROW_TEMP_EXPLOSION_DAMAGE = 3D;
    @Save({"arrow", "tempexplosion", "radius"})
    public static double ARROW_TEMP_EXPLOSION_RADIUS = 3D;
    @Save({"arrow", "tempexplosion", "duration"})
    public static long ARROW_TEMP_EXPLOSION_DURATION = 15000L;
    @Save({"arrow", "tempexplosion", "power"})
    public static double ARROW_TEMP_EXPLOSION_POWER = 2D;

    @Save({"arrow", "liteexplosion", "name"})
    public static String ARROW_LITE_EXPLOSION_NAME = "§5§lСлабая взрывная стрела";
    @Save({"arrow", "liteexplosion", "lore"})
    public static List<String> ARROW_LITE_EXPLOSION_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lВзрывается при столкновении"
            , "§f§lВзрыв не способен разрушить територию");
    @Save({"arrow", "liteexplosion", "damage"})
    public static double ARROW_LITE_EXPLOSION_DAMAGE = 1D;
    @Save({"arrow", "liteexplosion", "radius"})
    public static double ARROW_LITE_EXPLOSION_RADIUS = 3D;
    @Save({"arrow", "liteexplosion", "power"})
    public static double ARROW_LITE_EXPLOSION_POWER = 1D;

    @Save({"arrow", "air", "name"})
    public static String ARROW_AIR_NAME = "§5§lВоздушная стрела";
    @Save({"arrow", "air", "lore"})
    public static List<String> ARROW_AIR_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lПо столкновении запускает цель в воздух");
    @Save({"arrow", "air", "power"})
    public static double ARROW_AIR_POWER = 1D;
    @Save({"arrow", "air", "replenish", "radius"})
    public static double ARROW_AIR_REPLENISH_RADIUS = 5D;
    @Save({"arrow", "air", "replenish", "power"})
    public static int ARROW_AIR_REPLENISH_POWER = 1;

    @Save({"arrow", "confusion", "name"})
    public static String ARROW_CONFUSION_NAME = "§5§lДезориентирующая стрела";
    @Save({"arrow", "confusion", "lore"})
    public static List<String> ARROW_CONFUSION_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lЭффект Confusion применяется к игроку"
            , "§f§lИгрока разворачивает на 180 градусов");
    @Save({"arrow", "confusion", "duration"})
    public static long ARROW_CONFUSION_DURATION = 5000L;

    @Save({"arrow", "darkness", "name"})
    public static String ARROW_DARKNESS_NAME = "§5§lНочная стрела";
    @Save({"arrow", "darkness", "lore"})
    public static List<String> ARROW_DARKNESS_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lВызывает эффект дымовой завесы");
    @Save({"arrow", "darkness", "duration"})
    public static long ARROW_DARKNESS_DURATION = 5000L;
    @Save({"arrow", "darkness", "radius"})
    public static double ARROW_DARKNESS_RADIUS = 5D;

    @Save({"arrow", "death", "name"})
    public static String ARROW_DEATH_NAME = "§5§lСмертельная стрела";
    @Save({"arrow", "death", "lore"})
    public static List<String> ARROW_DEATH_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lНакладывает на цель эффект иссушения."
            , "§f§lС некоторым шансом, цель погибает");
    @Save({"arrow", "death", "duration"})
    public static long ARROW_DEATH_DURATION = 5000L;
    @Save({"arrow", "death", "power"})
    public static int ARROW_DEATH_POWER = 2;
    @Save({"arrow", "death", "chance"})
    public static int ARROW_DEATH_CHANCE = 20;

    @Save({"arrow", "earth", "name"})
    public static String ARROW_EARTH_NAME = "§5§lЗемляная стрела";
    @Save({"arrow", "earth", "lore"})
    public static List<String> ARROW_EARTH_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lСковывает движение противника в земле.");
    @Save({"arrow", "earth", "duration"})
    public static long ARROW_EARTH_DURATION = 5000L;

    @Save({"arrow", "ender", "name"})
    public static String ARROW_ENDER_NAME = "§5§lЭндер стрела";
    @Save({"arrow", "ender", "lore"})
    public static List<String> ARROW_ENDER_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lТелепортирует вас в место падения"
            , "§f§lМеняет местами с другим существом");

    @Save({"arrow", "grapple", "name"})
    public static String ARROW_GRAPPLE_NAME = "§5§lХваткая стрела";
    @Save({"arrow", "grapple", "lore"})
    public static List<String> ARROW_GRAPPLE_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lТелепортирует вас в место падения"
            , "§f§lМеняет местами с другим существом");
    @Save({"arrow", "grapple", "disable_fall_damage"})
    public static boolean ARROW_GRAPPLE_DISABLE_FALL_DAMAGE = true;

    @Save({"arrow", "life", "name"})
    public static String ARROW_LIFE_NAME = "§5§lЖизненная стрела";
    @Save({"arrow", "life", "lore"})
    public static List<String> ARROW_LIFE_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lРаспыляет эффект регенерации");
    @Save({"arrow", "life", "duration"})
    public static long ARROW_LIFE_DURATION = 5000L;
    @Save({"arrow", "life", "power"})
    public static int ARROW_LIFE_POWER = 2;
    @Save({"arrow", "life", "radius"})
    public static double ARROW_LIFE_RADIUS = 3;

    @Save({"arrow", "magic", "name"})
    public static String ARROW_MAGIC_NAME = "§5§lМагичесская стрела";
    @Save({"arrow", "magic", "lore"})
    public static List<String> ARROW_MAGIC_LORE = Arrays.asList("§f§lИнформация:"
            , "§f§lСильная отдача");

    public Configuration(Plugin plugin, String name) {
        super(plugin, name);
    }
}
