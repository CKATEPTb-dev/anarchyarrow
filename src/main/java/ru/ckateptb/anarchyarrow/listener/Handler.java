package ru.ckateptb.anarchyarrow.listener;

import ru.ckateptb.anarchyarrow.arrow.AnarchyArrowFactory;
import ru.ckateptb.anarchyarrow.arrow.IAnarchyArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.projectiles.ProjectileSource;

public class Handler implements Listener {
    @EventHandler
    public void on(ProjectileLaunchEvent event) {
        if (event.getEntity() instanceof Arrow) {
            Arrow arrow = (Arrow) event.getEntity();
            ProjectileSource source = arrow.getShooter();
            if (source instanceof Player) {
                Player player = (Player) source;
                AnarchyArrowFactory.createByFirstArrow(player,arrow).ifPresent(IAnarchyArrow::register);
            }
        }
    }

    @EventHandler
    public void on(ProjectileHitEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof Arrow) {
            AnarchyArrowFactory.get((Arrow) entity).ifPresent(anarchyArrow -> anarchyArrow.onHit(event));
        }
    }

    @EventHandler
    public void on(EntityDamageByEntityEvent event) {
        if(event.getEntity() instanceof FallingBlock) {
            event.setCancelled(true);
            return;
        }
        Entity damager = event.getDamager();
        if (damager instanceof Arrow) {
            AnarchyArrowFactory.get((Arrow) damager).ifPresent(anarchyArrow -> anarchyArrow.onHit(event));
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void on(PlayerPickupArrowEvent event) {
        AnarchyArrowFactory.get(event.getArrow()).ifPresent(anarchyArrow -> anarchyArrow.onPickUp(event));
    }
}
